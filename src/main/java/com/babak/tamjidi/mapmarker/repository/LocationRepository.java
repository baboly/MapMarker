package com.babak.tamjidi.mapmarker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.babak.tamjidi.mapmarker.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer> {
}
