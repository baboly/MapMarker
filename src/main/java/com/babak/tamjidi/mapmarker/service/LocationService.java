package com.babak.tamjidi.mapmarker.service;

import java.util.List;

import com.babak.tamjidi.mapmarker.entity.Location;

public interface LocationService {
	 void saveLocations();

	 List<Location> getAll();
	
	 Object[][] getLocationsAsObjectArray();
}