package com.babak.tamjidi.mapmarker.service;

import java.util.ArrayList;
import java.util.List;

import com.babak.tamjidi.mapmarker.utils.JsonFileParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.babak.tamjidi.mapmarker.entity.Location;
import com.babak.tamjidi.mapmarker.repository.LocationRepository;

@Component
public class LocationServiceImpl implements LocationService {

	private LocationRepository locationRepository;
	
	@Autowired
	public LocationServiceImpl(LocationRepository locationRepository){
		this.locationRepository = locationRepository;
	}
	
	@Override
	public void saveLocations() {
	   JsonFileParser jsonFileParser = new JsonFileParser();
	   List<Location> locations = new ArrayList<Location>();
	   try {
		locations = jsonFileParser.JsonParser();
	} catch (Exception e) {
		e.printStackTrace();
	}
	    for(Location location : locations){	
	    	locationRepository.save(location);
	    }
	}
	
	@Override
	public List<Location> getAll() {
		return locationRepository.findAll();
	}

	@Override
	public Object[][] getLocationsAsObjectArray() {
		List<Location> locationList = locationRepository.findAll();
		Object[][] locationArray = new Object[locationList.size()][locationList.size()];
		for (int i = 0; i < locationList.size(); i++) {
		    locationArray[i] = locationList.get(i).toObjectArray();
		}	
		return locationArray;
	}

}
